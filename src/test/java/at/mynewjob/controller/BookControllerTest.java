package at.mynewjob.controller;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import at.mynewjob.domain.Book;
import at.mynewjob.repositories.BookRepository;

public class BookControllerTest {
	
	private BookRepository repo;
	private BookController controller = new BookController();
	
	@Before
	public void setup() {
		repo= Mockito.mock(BookRepository.class);
		controller.setRepo(repo);
	}

	@Test
	public void testNonexistentBook() {
		Model model = new ExtendedModelMap();
		String result = controller.showBook(10, model);
		assertEquals("redirect:/books", result);
	}
	
	@Test
	public void testBook() {
		long id=10;
		Mockito.when(repo.find(id)).thenReturn(
				new Book("isbn", "title", new BigDecimal("10")));
		Model model = new ExtendedModelMap();
		String result = controller.showBook(id, model);
		assertEquals("book", result);
	}
	
	@Test
	public void testEmptyBookList() {
		Model model = Mockito.mock(Model.class);
		String result = controller.showBookList(0, 10, model);
		assertEquals("booklist", result);
		Mockito.verify(model).addAttribute("count", 0);
	}
}
