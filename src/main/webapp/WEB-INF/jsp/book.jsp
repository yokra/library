<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">

<head>
<title><c:out value="${book.title}"/></title>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

</head>


<body>

<h1>
<c:if test="${isCreation}"><spring:message code="createNewBook"/></c:if>
<c:out value="${book.title}"/>
</h1>

<form:form commandName="book">
<table class="book center">
<tr>
	<td><spring:message code="isbn"/>: </td>
	<td><form:input path="isbn" /></td>
	<td class="error"><form:errors path="isbn" /></td>
</tr>
<tr>
	<td><spring:message code="title"/>: </td>
	<td><form:input path="title" size="50" /></td>
	<td class="error"><form:errors path="title" /></td>
</tr>
<tr>
	<td><spring:message code="price"/>: </td>
	<td><form:input path="price" /></td>
	<td class="error"><form:errors path="price" /></td>
</tr>
<tr>
	<td colspan="3">
		<form:hidden path="id" />
		<spring:message code="save" var="saveText"/>
		<input type="submit" value="${saveText}" class="tableButton" />
		<spring:message code="cancel" var="cancelText"/>
		<input type="button" value="${cancelText}" onclick="javascript:window.history.back()" class="tableButton" />
	</td>
</tr>
</table>
</form:form>

<br/>

</body>

</html>