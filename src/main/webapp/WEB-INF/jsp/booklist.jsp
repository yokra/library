<!DOCTYPE html >
<%@page contentType="text/html;charset=UTF-8;" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">

<head>

<title>Books</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="_csrf" content="${_csrf.token}"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script>
	var selectedBook = -1;
	function populate() {
		var token = $("meta[name='_csrf']").attr("content");
		$.ajax({
			type: "GET",
			headers: {"X-CSRF-TOKEN" : token},
			url: "books/insertDemoBooks",
		})
		.done(function( msg ) {
			console.log("populated");
			location.reload();
		});
	}
	function askDelete(id) {
		selectedBook = id;
		$("#dlgConfirmDelete").dialog("open")
	}
	function doDelete() {
		var bookToDelete = selectedBook;
		var token = $("meta[name='_csrf']").attr("content");
		$.ajax({
			type: "DELETE",
			headers: {"X-CSRF-TOKEN" : token},
			url: "books/" + bookToDelete,
		})
		.done(function() {
			$("#row" + bookToDelete).hide("slow");
		})
		.fail(function() {
			console.log("deletion failed");
			$("#dlgDeleteError").dialog("open")
		});
	}
	$(function() {
		$("#dlgConfirmDelete").dialog({
			autoOpen: false,
			resizable : false,
			height : 200,
			width : 400,
			modal : true,
			buttons : {
				<spring:message code="delete"/> : function() {
					$(this).dialog("close");
					doDelete();
				},
				<spring:message code="cancel"/> : function() {
					$(this).dialog("close");
				}
			}
		});
	});
	$(function() {
		$("#dlgDeleteError").dialog({autoOpen: false});
	});
</script>
</head>


<body>

<h1><spring:message code="books"/></h1>


<table class="booklist center">
<tr>
	<th><spring:message code="isbn"/></th>
	<th><spring:message code="title"/></th>
	<th><spring:message code="price"/></th>
	<th/>
</tr>
<c:forEach var="book" items="${books}">
	<tr id="row${book.id}">
		<td><c:out value="${book.isbn}"/></td>
		<td>
			<c:if test="${showCreate}">
				<a href="/books/${book.id}"><c:out value="${book.title}"/></a>
			</c:if>
			<c:if test="${not showCreate}"><c:out value="${book.title}"/></c:if>
		</td>
		<td style="text-align:right"><c:out value="${book.formattedPrice} €"/></td>
		<td><c:if test="${showDelete}">
			<button onclick="askDelete(${book.id})">x</button>
		</c:if></td>
	</tr>
</c:forEach>

<tr class="pager"><td colspan="4">
	<c:if test="${not hasBefore}">«</c:if>
	<c:url value="books?start=${beforeStart}&max=${max}" var="link"/>
	<c:if test="${hasBefore}"><a href="${link}">«</a></c:if>
	
	<c:out value="${from}"/> - <c:out value="${to}"/> (<c:out value="${count}"/>)
	
	<c:if test="${not hasAfter}">»</c:if>
	<c:url value="books?start=${afterStart}&max=${max}" var="link"/>
	<c:if test="${hasAfter}"><a href="${link}">»</a></c:if>
</td></tr>
</table>

<br/>

<c:if test="${showLogin}"><a class="taskLink" href="/login">Login</a></c:if>
<c:if test="${showCreate}">
	<a class="taskLink" href="/books/create"><spring:message code="createNewBook"/></a>
</c:if>
<a class="taskLink" href="javascript:populate()"><spring:message code="populate"/></a>


<spring:message code="delete" var="dlgTitle"/>
<div id="dlgConfirmDelete" title="${dlgTitle}">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
<spring:message code="deleteQuestion"/></p>
</div>

<spring:message code="deleteErrorTitle" var="dlgErrorTitle"/>
<div id="dlgDeleteError" title="${dlgErrorTitle}">
<p><spring:message code="deleteErrorText"/></p>
</div>

</body>

</html>