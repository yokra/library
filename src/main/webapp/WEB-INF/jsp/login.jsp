<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">

<head>
<title>Login</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>


<body>

<h1>Login</h1>

<c:url value="/login" var="loginProcessingUrl" />
<form action="${loginProcessingUrl}" method="post">
	<fieldset class="center" style="width:500px">
		<legend><spring:message code="pleaseLogin"/></legend>
		<!-- use param.error assuming FormLoginConfigurer#failureUrl contains the query parameter error -->
		<c:if test="${param.error != null}">
			<div class="error"><spring:message code="loginFailed"/></div>
		</c:if>
		<!-- the configured LogoutConfigurer#logoutSuccessUrl is /login?logout and contains the query param logout -->
		<c:if test="${param.logout != null}">
			<div><spring:message code="loggedOut"/></div>
		</c:if>
		<p>
			<label for="username"><spring:message code="userName"/> (admin):</label>
			<input type="text" id="username" name="username" />
		</p>
		<p>
			<label for="password"><spring:message code="password"/> (admin):</label> 
			<input type="password" id="password" name="password" />
		</p>
		<div>
			<input type="hidden" id="_csrf" name="_csrf" value="${_csrf.token}" />
			<button type="submit">Log in</button>
			<spring:message code="cancel" var="cancelText"/>
		<input type="button" value="${cancelText}" onclick="javascript:window.history.back()" />
		</div>
	</fieldset>
</form>

</body>

</html>