package at.mynewjob.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import at.mynewjob.Application;
import at.mynewjob.domain.Book;
import at.mynewjob.repositories.BookRepository;

@Controller
public class BookController {

	private static final Logger log = Logger.getLogger(BookController.class);
	
	@Autowired
	private BookRepository bookRepo;
	
	void setRepo(BookRepository bookRepo) {
		this.bookRepo = bookRepo;
	}
	
	@RequestMapping(value="/books", method=RequestMethod.GET)
	public String showBookList(Integer start, Integer max, Model model) {
		log.info("showing books");
		if (start == null || start < 0)
			start = 0;
		if (max == null || max <= 0)
			max = 10;
		
		int totalCount = bookRepo.count();
		if (start >= totalCount)
			start = Math.max(0, totalCount - max);
		
		List<Book> books = bookRepo.findEntries(start, max);
		
		model.addAttribute("books", books);
		int from = totalCount > 0 ? start+1 : 0;
		model.addAttribute("from", from);
		model.addAttribute("to", start + books.size());
		model.addAttribute("count", totalCount);
		model.addAttribute("max", max);
		
		model.addAttribute("hasBefore", start > 0);
		model.addAttribute("beforeStart", Math.max(0, start - max));
		
		model.addAttribute("hasAfter", start + max < totalCount);
		model.addAttribute("afterStart", start + books.size());
		
		addLoggedInStatus(model);
		
		return "booklist";
	}
	
	private void addLoggedInStatus(Model model) {
		if (! Application.currentUserIsAdmin())
			model.addAttribute("showLogin", true);
		else {
			model.addAttribute("showCreate", true);
			model.addAttribute("showEdit", true);
			model.addAttribute("showDelete", true);
		}
	}

	@RequestMapping(value="/books/create", method=RequestMethod.GET)
	public String showBookCreation(Model model) {
		log.info("book creation");
		model.addAttribute("book", new Book("", "", null));
		model.addAttribute("isCreation", true);
		return "book";
	}
	
	@RequestMapping(value="/books/create", method=RequestMethod.POST)
	public String createBook(@Valid Book book, BindingResult result, Model model) {
		log.info("create book");
		if (result.hasErrors()) {
			model.addAttribute("book", book);
			return "book";
		}
		bookRepo.persist(book);
		return "redirect:/books";
	}
	
	@RequestMapping(value="/books/{id}", method=RequestMethod.GET)
	public String showBook(@PathVariable long id, Model model) {
		log.info("showing book " + id);
		Book book = bookRepo.find(id);
		if (book == null)
			return "redirect:/books";
		model.addAttribute("book", book);
		return "book";
	}
	
	@RequestMapping(value="/books/{id}", method=RequestMethod.POST)
	public String changeBook(@PathVariable long id, @Valid Book book, BindingResult result, Model model) {
		log.info("changing book " + id);
		if (id != book.getId())
			throw new RuntimeException("no book/wrong id");
		if (result.hasErrors()) {
			model.addAttribute("book", book);
			return "book";
		}
		bookRepo.merge(book);
		return "redirect:/books";
	}
	
	@RequestMapping(value="/books/{id}", method=RequestMethod.DELETE)
	public void deleteBook(@PathVariable long id, HttpServletResponse response) throws IOException {
		log.info("deleting book " + id);
		bookRepo.remove(id);
		PrintWriter writer = response.getWriter();
		writer.println("{}");
	}
	
	@RequestMapping(value="/books/insertDemoBooks", method=RequestMethod.GET)
	public void insertDemoBooks(HttpServletResponse response) throws IOException {
		log.info("inserting demo books");
		bookRepo.insertDemoBooks();
		PrintWriter writer = response.getWriter();
		writer.println("done");
	}
}
