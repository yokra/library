package at.mynewjob.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.number.NumberFormatter;

/**
 * A book in the library.
 * @author andreas
 */
@Entity
public class Book implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String CURRENCY_PATTERN = "#,##0.00";

	@Id
	@GeneratedValue
	private Long id;
	
	@Size(min=10, max=17)
	private String isbn;
	
	@Size(min=1, max=255)
	private String title;
	
	@Min(0)
	@Digits(fraction=2, integer=8)
	@NumberFormat(pattern=CURRENCY_PATTERN)
	private BigDecimal price;
	
	Book() {
		// default constructor for Hibernate
	}
	
	public Book(String isbn, String title, BigDecimal price) {
		super();
		this.isbn = isbn;
		this.title = title;
		this.price = price;
	}
	
	public String getFormattedPrice() {
		if (price == null)
			return "";
		return new NumberFormatter(CURRENCY_PATTERN)
			.print(price, LocaleContextHolder.getLocale());
	}
	
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "Book [id=" + id + ", title=" + title + "]";
	}
	
}
