package at.mynewjob.repositories;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.mynewjob.domain.Book;

/**
 * Gives access to persisted books.
 * @author andreas
 */
@Repository
@Transactional
public class BookRepository {
	
	private static final Logger log = Logger.getLogger(BookRepository.class);

	@PersistenceContext
	private EntityManager entityManager;
	
	public int count() {
		return entityManager.createQuery("select count(b) from Book b", 
				Long.class).getSingleResult().intValue();
	}
	
	/**
	 * inserts some demo books into the DB
	 */
	public void insertDemoBooks() {
		Book[] books = {
				new Book("978-4324430054", "Capital in the 21st Century", new BigDecimal(25)),
				new Book("123-8654430006", "Software Architecture", new BigDecimal("32.20")),
				new Book("987-2354430506", "Mahatma Ghandi", new BigDecimal("10.20")),
				new Book("834-5844430203", "Komm süßer Tod", new BigDecimal("17.90")),
				new Book("125-2354430034", "Kevin Mitnick - Ghost in the wires", new BigDecimal("32.2")),
				new Book("234-9874480345", "Design Patterns", new BigDecimal("12.90")),
				new Book("436-2354439763", "Clean Code", new BigDecimal("3000")),
				new Book("234-9874480345", "Design Patterns", new BigDecimal("12.90")),
				new Book("922-9874480345", "Lord of the Rings", new BigDecimal("12.90")),
				new Book("234-9874480345", "The Bible", new BigDecimal("12.90")),
				new Book("526-9874480345", "Spring Boot for Dummies", new BigDecimal("8.90")),
				new Book("234-9874480345", "The Countdown", new BigDecimal("12.90")),
		};
		for (Book book : books) {
			entityManager.persist(book);
		}
	}
	
	/**
	 * @param id the id of the book
	 * @return the book or null if not found
	 */
	public Book find(long id) {
		return entityManager.find(Book.class, id);
	}
	
	public List<Book> findAll() {
		return entityManager.createQuery("select b from Book b order by b.title", 
				Book.class).getResultList();
	}
	
	/**
	 * @param start index of first book, starting with 0
	 * @param count maximum number of books that should be returned
	 * @return an ordered subset of the persisted books
	 */
	public List<Book> findEntries(int start, int count) {
		return entityManager.createQuery("select b from Book b order by b.title", 
				Book.class).setFirstResult(start).setMaxResults(count).getResultList();
	}

	/**
	 * removes a book by id. does nothing if the book does not exist.
	 */
	public void remove(long id) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaDelete<Book> delete = cb.createCriteriaDelete(Book.class);
		Root<Book> root = delete.from(Book.class);
		delete.where(cb.equal(root.get("id"), id));
		int count = entityManager.createQuery(delete).executeUpdate();
		log.info("deleted: " + count);
	}
	
	public void merge(Book book) {
		entityManager.merge(book);
	}

	/**
	 * Persists a new book.
	 * @param book the book may not have an ID already
	 */
	public void persist(Book book) {
		if (book.getId() != null)
			throw new IllegalArgumentException("book already has an ID");
		entityManager.persist(book);
	}
}
