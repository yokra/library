= how to run =

* within eclipse: run at.mynewjob.Application
* on the command line: 
	mvn package
	java -jar target/library-0.0.1-SNAPSHOT.war
* it should also work as a war-file deployed to a web server (not tested)

then open http://localhost:8080/books 


= i18n =

i18n depends on the browser-header (Accept-Language). 
to see the english version, change your browsers's settings.

= database =

an h2 in-memory database is used. your data is lost after shutdown.


= next steps =

- more unit tests for the controller
- isbn pattern checker
- language switch by url
- update of pager data after delete
- i18n of failed currency parsing
- add logout link
- remember last page after login
- catch session timeout on book deletion
- caching of the css in the browser
- make LibraryIntegrationTest run in mvn test